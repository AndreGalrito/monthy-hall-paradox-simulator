# Monthy Hall Paradox Simulator#
* v1.0 (2010/06/04)

### What is this repository for? ###

* A small and fun project that simulates a given amount of scenarios with parallel results, one where the challenged person stuck with the first decision and another where the same didn't. You'll see that the bigger amount of attempts the more accurate the result will be, proving that indeed the paradox is right. Never stick with your first choice.
[Monthy Hall problem - Wikipedia](https://en.wikipedia.org/wiki/Monty_Hall_problem)

### How do I get set up? ###

* Simply run the provided .jar file.