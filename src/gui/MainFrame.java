package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import model.Simulator;

public class MainFrame
{

	private JFrame		frmMonthyHallParadox;
	private Simulator	simulator;
	private JTextField textField;
	private int nSets;
	private JTextArea	textArea;
	private JTextArea	textArea_1;

	/**
	 * Create the application.
	 */
	public MainFrame(Simulator sim)
	{
		this.simulator = sim;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
	    this.frmMonthyHallParadox = new JFrame();
	    this.frmMonthyHallParadox.setVisible(true);
	    this.frmMonthyHallParadox.setTitle("Monthy Hall Paradox Simulator");
	    this.frmMonthyHallParadox.setBounds(100, 100, 873, 440);
	    this.frmMonthyHallParadox.setDefaultCloseOperation(3);
	    this.frmMonthyHallParadox.getContentPane().setLayout(null);
	    
	    JButton btnSimulate = new JButton("Simulate");
	    btnSimulate.addActionListener(new ActionListener()
	    {
	      public void actionPerformed(ActionEvent arg0)
	      {
	        MainFrame.this.nSets = Integer.parseInt(MainFrame.this.textField.getText());
	        MainFrame.this.simulator.setN_Sets(MainFrame.this.nSets);
	        MainFrame.this.printReports();
	      }
	    });
	    btnSimulate.setBounds(312, 348, 200, 40);
	    this.frmMonthyHallParadox.getContentPane().add(btnSimulate);
	    
	    JLabel lblStickWithst = new JLabel("Stick with 1st choice scenario");
	    lblStickWithst.setBounds(10, 11, 237, 14);
	    this.frmMonthyHallParadox.getContentPane().add(lblStickWithst);
	    
	    JLabel lblSwitchChoiceScenario = new JLabel("Switch choice scenario");
	    lblSwitchChoiceScenario.setBounds(413, 11, 237, 14);
	    this.frmMonthyHallParadox.getContentPane().add(lblSwitchChoiceScenario);
	    
	    this.textField = new JTextField();
	    this.textField.setHorizontalAlignment(4);
	    this.textField.setText("100");
	    this.textField.setBounds(522, 356, 61, 20);
	    this.frmMonthyHallParadox.getContentPane().add(this.textField);
	    this.textField.setColumns(10);
	    
	    JLabel lblsimulations = new JLabel("#simulations");
	    lblsimulations.setBounds(593, 359, 86, 14);
	    this.frmMonthyHallParadox.getContentPane().add(lblsimulations);
	    
	    JScrollPane scrollPane = new JScrollPane();
	    scrollPane.setEnabled(false);
	    scrollPane.setBounds(10, 30, 400, 311);
	    this.frmMonthyHallParadox.getContentPane().add(scrollPane);
	    
	    this.textArea = new JTextArea();
	    this.textArea.setFont(new Font("Tahoma", 0, 11));
	    this.textArea.setBackground(Color.BLACK);
	    this.textArea.setForeground(Color.WHITE);
	    scrollPane.setViewportView(this.textArea);
	    
	    JScrollPane scrollPane_1 = new JScrollPane();
	    scrollPane_1.setBounds(411, 30, 435, 311);
	    this.frmMonthyHallParadox.getContentPane().add(scrollPane_1);
	    
	    this.textArea_1 = new JTextArea();
	    this.textArea_1.setFont(new Font("Tahoma", 0, 11));
	    this.textArea_1.setBackground(Color.BLACK);
	    this.textArea_1.setForeground(Color.WHITE);
	    scrollPane_1.setViewportView(this.textArea_1);
	    this.simulator = new Simulator();
		}
	
	private void printReports()
	{
		this.textArea.setText("");
		this.textArea_1.setText("");
		for(int set = 0; set < this.nSets; set++)
		{
			this.textArea.append(this.simulator.generateStickReport(set));
			this.textArea_1.append(this.simulator.generateSwitchReport(set));
		}
		this.textArea.append("\n" + this.simulator.getStickWins() + " wins");
		this.textArea_1.append("\n" + this.simulator.getSwitchWins() + " wins");
		this.textArea.append("\n" + this.nSets + " simulations generated with success.");
		this.textArea_1.append("\n" + this.nSets + " simulations generated with success.");
		
		this.textArea.append("\n" + ((this.simulator.getStickWins() * 100) / this.nSets) + "% chance of winning.");
		this.textArea_1.append("\n" + ((this.simulator.getSwitchWins() * 100) / this.nSets) + "% chance of winning.");
	}
}
