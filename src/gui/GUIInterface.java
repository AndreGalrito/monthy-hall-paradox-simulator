package gui;

import javax.swing.JFrame;

import model.Simulator;

/**
 * Handles the starting of the application
 * @author Andr� Galrito
 * @version 2010/06/04
 */
@SuppressWarnings("serial")
public class GUIInterface extends JFrame
{
   /**
    * Constructor for class GUIInterface
    * @param args
    */
   public static Simulator initialData()
   {
	  Simulator sim = new Simulator();
      return sim;
   }
  
   /**
    * Constructor for class GUIInterface
    * @param args
    */
   public static void main(String[] args)
   {
      javax.swing.SwingUtilities.invokeLater(new Runnable()
      {
         public void run()
         {
            Simulator sim = GUIInterface.initialData();
            new MainFrame(sim);
         }
      });
   }
}

//END of class GUIInterface
