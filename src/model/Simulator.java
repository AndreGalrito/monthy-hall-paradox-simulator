package model;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class Simulator
{
	private List<DoorSet>	doorSets;
	private List<Integer>	randomnessPrize;
	private List<Integer>	randomnessChoice;
	private List<Integer>	switchChoice;
	private List<Integer>	doorsOpen;
	private List<Boolean>	stickResult;
	private List<Boolean>	switchResult;
	private int				n_sets;
	private int 			stickWins;
	private int 			switchWins;

	public Simulator()
	{
		this.n_sets = 100;
		this.stickWins = 0;
		this.switchWins = 0;
		this.generateRandomnessPrize();
		this.generateRandomnessChoice();
		this.generateDoorSets();
		this.generateDoorOpen();
		this.generateStickResult();
		this.generateSwitchChoice();
		this.generateSwitchResult();

		// this.testThis();
	}
	
	public void setN_Sets(int n)
	{
		this.n_sets = n;
		this.stickWins = 0;
		this.switchWins = 0;
		this.generateRandomnessPrize();
		this.generateRandomnessChoice();
		this.generateDoorSets();
		this.generateDoorOpen();
		this.generateStickResult();
		this.generateSwitchChoice();
		this.generateSwitchResult();
	}

	public int getStickWins()
	{
		return this.stickWins;
	}
	
	public int getSwitchWins()
	{
		return this.switchWins;
	}
	
	public String generateSwitchReport(int set)
	{
		@SuppressWarnings("resource")
		Formatter fmt = new Formatter();

		fmt.format(
				"Prize behind door %d. I choose door %d. Host opens door %d. Switch to door %d. Win? %b\n",
				this.doorSets.get(set).getPrizeDoor(),
				this.randomnessChoice.get(set), this.doorsOpen.get(set),
				this.switchChoice.get(set), this.switchResult.get(set));
		String report = "" + fmt;
		return report;
	}

	public String generateStickReport(int set)
	{
		@SuppressWarnings("resource")
		Formatter fmt = new Formatter();

		fmt.format(
				"Prize behind door %d. I choose door %d. Host opens door %d. I stick! Win? %b\n",
				this.doorSets.get(set).getPrizeDoor(),
				this.randomnessChoice.get(set), this.doorsOpen.get(set),
				this.stickResult.get(set));
		String report = "" + fmt;
		return report;

	}

	private void generateSwitchResult()
	{
		this.switchResult = new ArrayList<Boolean>();
		for (int doorSet = 0; doorSet < this.n_sets; doorSet++)
		{
			if (this.doorSets.get(doorSet).getPrizeDoor() == this.switchChoice.get(doorSet))
			{
				this.switchResult.add(doorSet, true);
				this.switchWins++;
			} else
			{
				this.switchResult.add(doorSet, false);
			}
		}

	}

	private void generateStickResult()
	{
		this.stickResult = new ArrayList<Boolean>();
		for (int doorSet = 0; doorSet < this.n_sets; doorSet++)
		{
			if (this.doorSets.get(doorSet).getPrizeDoor() == this.randomnessChoice
					.get(doorSet))
			{
				this.stickResult.add(true);
				this.stickWins++;
			} else
			{
				this.stickResult.add(false);
			}
		}

	}

	private void generateDoorOpen()
	{
		this.doorsOpen = new ArrayList<Integer>();
		final int N_DOORS = 3;

		for (int doorSet = 0; doorSet < this.n_sets; doorSet++)
		{
			for (int door = 1; door <= N_DOORS; door++)
			{
				if ((door == this.randomnessChoice.get(doorSet))
						|| (door == this.doorSets.get(doorSet).getPrizeDoor()))
				{
					// do nothing
				} else
				{
					this.doorsOpen.add(doorSet, door);
				}

			}
		}

	}

	private void generateDoorSets()
	{
		this.doorSets = new ArrayList<DoorSet>();
		for (int doorSet = 0; doorSet < this.n_sets; doorSet++)
		{
			this.doorSets.add(new DoorSet(this.randomnessPrize.get(doorSet)));
		}

	}

	private void generateRandomnessPrize()
	{
		this.randomnessPrize = new ArrayList<Integer>();
		for (int set = 0; set < this.n_sets; set++)
		{
			final int PRIZE_DOOR = 1 + (int) (Math.random() * 3);
			this.randomnessPrize.add(PRIZE_DOOR);
		}
	}

	private void generateSwitchChoice()
	{
		this.switchChoice = new ArrayList<Integer>();
		final int N_DOORS = 3;
		for (int set = 0; set < this.n_sets; set++)
		{
			for (int door = 1; door <= N_DOORS; door++)
			{
				if ((door == this.randomnessChoice.get(set))
						|| door == this.doorsOpen.get(set))
				{
					// do nothing
				} else
				{
					this.switchChoice.add(set, door);
				}
			}
		}

	}

	private void generateRandomnessChoice()
	{
		this.randomnessChoice = new ArrayList<Integer>();
		for (int set = 0; set < this.n_sets; set++)
		{
			final int DOOR_PICKED = 1 + (int) (Math.random() * 3);
			this.randomnessChoice.add(DOOR_PICKED);
		}
	}

}
