package model;

public class Door
{
	private boolean hasPrize;

	public Door()
	{
		this.hasPrize = false;
	}
	
	public void setPrize()
	{
		this.hasPrize = true;
	}
	
	public boolean hasPrize()
	{
		return this.hasPrize;
	}
}
