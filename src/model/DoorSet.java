package model;

import java.util.ArrayList;
import java.util.List;

public class DoorSet
{
	private List<Door> doors;
	final private int N_DOORS = 3;
	private int prizeDoor;
	
	public DoorSet(int prizeDoor)
	{
		this.prizeDoor = prizeDoor;
		this.initDoors();
	}
	
	private void initDoors()
	{
		this.doors = new ArrayList<Door>();
		for(int door = 0; door < N_DOORS; door++)
		{
			this.doors.add(new Door());
			if(door == prizeDoor - 1)
			{
				this.doors.get(door).setPrize();
			}
		}
	}
	
	public List<Door> getDoors()
	{
		return this.doors;
	}
	
	public int getPrizeDoor()
	{
		int prizeDoor = 1;
		for(int door = 0; door < this.doors.size(); door++)
		{
			if(this.doors.get(door).hasPrize())
			{
				prizeDoor += door;
			}
		}
		return prizeDoor;
	}
}
